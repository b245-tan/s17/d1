// console.log("Hello World");

// [SECTION] Functions
	// Functions in JavaScript are line/blocks of codes that tell our device/application to perform a specific task when called/invoked.
	// It prevents repeating lines/blocks of codes that perform the same task/function.

	// Function Declarations
		// Function statement is the definition of a function.
	
	/*
		Syntax:
			function functionName() {
				code block //statement
			}

		- function keyword is used to define a javascript function.
		- functionName is the name of the function which will be used to call/invoke the function.
		- fucntion block({}) - indicates the function body.
	*/

	function printName(){
		console.log("My name is John");
	}

	// Function Invocation
		//this invokes the code block inside the function
		// This will run/execute the code block inside the function when call/invoked
	printName();

	// declaredFunction(); //need to declare first.

// [SECTION] Function Declaration vs Function Expressions
	
	// Function Declaration
		// function can be created by using 'function' keyword and adding a function name.
		// saved for later use
		// Declared functions can be "hoisted", as long as a function has been defined.
			// Hoisting is JS behavior for certain varaible ("var") and functions to run or use them before their declaration.

	function declaredFunction() {
		console.log("Hello World from declaredFunction()");
	}
	declaredFunction()

	// Function Expression
		// A function can also be stored in a variable.

		/*
			Syntax:
				let/const variableName = function() {
					//code block 
				}

				-function(){} - Anonymous function, a function without a name.
		*/

		// variableFunction(); //error - function expression, being stored in a let/const variable cannot be hoisted.

		let variableFunction = function() {
			console.log("Hello Again!")
		}

		variableFunction();

		// We can also create function expression with a named function.

		let funcExpression = function funcName() {
			console.log("Hello from the other side.");
		}

		// funcName(); //is not defined
		funcExpression(); //to invoke the function we invoke it by its 'variable name' not by its 'function name'. 

		// Reassign declared functions and function expression 

		declaredFunction = function() {
			console.log("Update declaredFunction");
		}
		declaredFunction();

		funcExpression = function() {
			console.log("Update funcExpression");
		}

		funcExpression();

		// We cannot re-assign a function expression initialized with const.

		const constantFunc = function() {
			console.log("Initialized with const");
		}

		constantFunc();

		/*

			constantFunc = function() {
				console.log("Cannot be reassigned");
			}

			constantFunc();

		*/

// [SECTION] Function Scoping
	/*
		Scope is the accessibility (visibility) of variables.

		JavaScript Variables has 3 types of scope:
		1. global scope
		2. local/block scope
		3. function scope
	*/

	// Global Scope
		// variable can be accessed anywhere in the program.

		let globalVar = "Mr. WorldWide";

	// Local Scope
		// variables declared inside a curly bracket ({}) can only be accessed locally.

		//console.log(localVar);

		{
			//var localVar = "Armando Perez";
			let localVar = "Armando Perez";
		}

		console.log(globalVar);
		//console.log(localVar); //error. cannot be accessed outside code block

		// Function Scope
			// Each Function creates a new scope.
			// Variables defined inside a function are not accessible from outside the function.

			function showNames(){
				var functionVar = "Joe";
				const functionConst = "John";
				let functionLet = "Joey";

				console.log(functionVar);
				console.log(functionConst);
				console.log(functionLet);
			}

			showNames();

			//console.log(functionVar);
			//console.log(functionConst);
			//console.log(functionLet);

			function myNewFunction(){
				let name = "Jane";

				function nestedFunc(){
					let nestedName = "John";
					console.log(name);
				}

				//console.log(nestedName);
				nestedFunc();
			}

			myNewFunction();

			// nestedFunc(); //error outside code block.

		// Global Scope Variable

			let globalName = "Alex";

			function myNewFunction2(){
				let nameInside = "Renz";
				console.log(globalName);
			}

			myNewFunction2();

			// console.log(nameInside); //only accessible on the function scope.

// [SECTION] Using alert() and prompt()
	// alert() allows us to show small window at the top of our browser page to show information to our users.

	// alert("Hello World!"); //This will run imediately when the page reloads

		// You can use an alert() to show a message to the user from a later function invocation.

		function showSampleAlert(){
			alert("Hello, User");
		}

		//showSampleAlert();

		console.log("I will only log in the console when the alert is dismissed.");

		// Notes on the use of alert();
			// Show only an alert() for short dialogues/messages to the user.
			// Do not overuse alert() because the program has to wait for it to be dismissed before the program continues.

	// prompt() allows us to show a small window at the top of the brower to gather user input.
		// The input from the prompt() will return as a "string" once the user dismisses the window.

		/*
			Syntax: 
			let/cost = prompt("<dialogueInString");
		*/

		// let name = prompt("Enter your name: ");
		// let age = prompt("Enter your age: ");

		// console.log(typeof age);
		// console.log("Hello, I am " + name + ". I am " + age + " years old.");

		// let sampleNullPrompt = prompt("Do Not Enter Anything");
		// console.log(sampleNullPrompt);
		// prompt() returns an "emptry string" ("") when there is no user input and we have clicked okay. Or "null" if the user cancels the prompt.

		function printWelcomeMessage() {
			let name = prompt("Enter your name: ");
			console.log("Hello, " + name + "! Welcome to my page!");
		}

		printWelcomeMessage();

// [SECTION] Function Naming Conventions
	// 1. Function names should be definitive of the task it will perform. It usually contains a verb.
		// Function also follows camelCase for naming 

		function getCourses(){
			let courses = ["Science 101", "Math 101", "English 101"];
			console.log(courses);
		}

		getCourses();

	// 2. Avoid generic names to avoid confusion within your code.

		function get() {
			let name = "Jamie";
			console.log(courses);
		}

		get();

	// 3. Avoid pointless and inappropriate function names. example: foo, bar, etc. (metasyntactic variables)

		function foo(){
			console.log(25%5);
		}